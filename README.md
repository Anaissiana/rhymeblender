**Rhyme Blender**

Rhyme Blender is a Java web application designed to mix various poems from different authors into new rhymed stanzas. Using UDAR (a morphological analyzer of the Russian language), Rhyme Blender can blend Russian poem lines depending on the orthoepic pronunciation standards concerning word stress.

**API**

User have to add input lines to be shuffle (this is a mandatory item). Use "poem" : "your poem here".
It is possible to specify the type of rhyme if necessary as well. Parallel stanzas strategy is default. 
To change the strategy use this format for cross stanzas:

"stanzasStrategy": "CROSS"

To make encircling stanzas:

"stanzasStrategy": "ENCIRCLING"

Moreover, user can apply his own stress map in case of author's stresses or rare words usage (which were not added by the UDAR library). If stress hasn't been placed in a word, the user will receive a list with such words to fix it.
The stress map is created in the format "word" : "stress index". Dealing with user stress map, keep in mind that index counting starts at zero. Indexes cannot include negative values, letters, or punctuation.

"accents": {
"пуньк": 1
}

**API calls examples**


- Only poem input

POST 

{

     "poem": "Так хорошо тогда мне вспоминать\n Что где-то у меня живут отец и мать,\n Теперь бы песню ветра\n И нежное баю\n За сотни километров\n Влила ты в грудь мою.\n"

}


- With an accent map

POST 

{

     "poem": "Так хорошо тогда мне вспоминать\n Что где-то у меня живут отец и мать,\n Теперь бы песню ветра\n И нежное баю\n За сотни километров\n Влила ты в грудь мою.\n"

     "accents": {
        "вспоминать": 7,
        "баю": 2
     }
}

- With an accent map and stanzas strategy

POST 

{
     
     "poem": "Так хорошо тогда мне вспоминать\n Что где-то у меня живут отец и мать,\n Теперь бы песню ветра\n И нежное баю\n За сотни километров\n Влила ты в грудь мою.\n"

     "accents": {
        "вспоминать": 7,
        "баю": 2
     }, 
     
     "stanzasStrategy": "CROSS"

}


**Error types**

- Poem Not Found Exception - poem not found, poem couldn't be empty
- Invalid Dictionary Word Exception - validation failed, invalid dictionary word input
- Invalid Accent Index Exception - validation failed, invalid accent index input


**HTTP Status code summary **

- 200 - OK, everything worked as expected
- 404 - Not Found, poem is empty
- 400 - Bad Request, invalid input 
- 500 - Server Errors, something went wrong 


**How to launch**

Firstly, UDAR library must be installed. Pay attention to non-python dependencies which can cause difficulties during the installation process.

For more info about requirements and scripts: https://gitlab.com/Anaissiana/rhymeblender/-/tree/master/python_stress

Python script path should be changed to the local one in application properties (accenter.python.scriptpath=<your_path_to_python_stress.sh>). 

Download UDAR: https://github.com/reynoldsnlp/udar

For the correct operation of the application databases, the local Postgres database should be used. Login and password is default (postgres/postgres)

**TODO list**

- Set up Spring Security
- Package python library into Docker container
- Start frontend development
- Add the ability to choose a poetic metre
- Create three-line stanzas strategy
