import udar
import sys
import stanza

if __name__ == '__main__':
    # run once
    # stanza.download('ru')
    path_in = sys.argv[1]
    path_out = sys.argv[2]
    file_in = open(path_in, 'r')
    input_str = file_in.read()
    input_str = input_str.replace("\n", "\n#%")
    in_doc = udar.Document(input_str, disambiguate=True)
    out = in_doc.stressed(selection='rand')  # 'rand' randomly chooses between сло́ва and слова́
    out = out.replace(" % ", "\n")
    out = out.replace("%", "")
    stressed_out = open(path_out, 'w')
    stressed_out.write(out)
