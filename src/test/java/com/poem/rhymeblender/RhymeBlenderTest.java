package com.poem.rhymeblender;

import com.poem.rhymeblender.accent.DictionaryHandler;
import com.poem.web.webdictionary.UserAccentDictionary;
import com.poem.web.webdictionary.WebDictHandler;
import org.junit.Before;
import org.junit.Test;

import static com.poem.rhymeblender.LangUtils.prepareLines;
import static org.junit.Assert.*;

import java.util.*;

public class RhymeBlenderTest {
    protected String noStressNoDict;
    protected String noStressAndDict;
    protected String allVariants;
    protected String rhymedVarDict;
    protected Map<String, List<String>> rhymedLines;
    protected Map<String, Integer> localDict;
    protected Set<String> withoutAccents;
    protected DictionaryHandler dictionaryHandler;
    protected UserAccentDictionary dictionary;

    @Before
    public void init() {
        noStressNoDict = "пуньк пуньк";
        noStressAndDict = "хрю";
        allVariants = String.join("\n", List.of(noStressAndDict, noStressNoDict));
        rhymedVarDict = String.join("\n", noStressAndDict, noStressAndDict, noStressNoDict);
        rhymedLines = Map.of("ю", List.of(noStressAndDict, noStressAndDict));
        localDict = Map.of(noStressAndDict, 2);
        dictionary = new UserAccentDictionary(localDict);
        dictionaryHandler = new WebDictHandler(dictionary);
        withoutAccents = Set.of("пуньк");
    }


    @Test
    public void getWithoutAccentsTest() {
        RhymeBlender rb2 = new RhymeBlender(dictionaryHandler, noStressAndDict);
        assertEquals(Collections.emptySet(), rb2.getWithoutAccents());
        RhymeBlender rb3 = new RhymeBlender(dictionaryHandler, noStressNoDict);
        assertEquals(withoutAccents, rb3.getWithoutAccents());
        RhymeBlender rb4 = new RhymeBlender(dictionaryHandler, allVariants);
        assertEquals(withoutAccents, rb4.getWithoutAccents());
    }

    @Test
    public void rhymeBlenderTest() {
        RhymeBlender rb = new RhymeBlender(dictionaryHandler, rhymedVarDict);
        assertEquals(withoutAccents, rb.getWithoutAccents());
        assertEquals(rhymedLines, rb.getRhymedLines());
    }
}