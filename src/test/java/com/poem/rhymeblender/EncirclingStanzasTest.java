package com.poem.rhymeblender;

import com.poem.rhymeblender.stanzas.EncirclingStanzas;
import com.poem.rhymeblender.stanzas.StanzasGenerationStrategy;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class EncirclingStanzasTest extends StanzasInit {
    public EncirclingStanzasTest() {
    }

    @Override
    public StanzasGenerationStrategy getStrategy() {
        return new EncirclingStanzas();
    }

    @Test
    public void makeEncirclingStanzasTest() {
        List<String> encirclingStanzas = getStanzas();
        Iterator<String> iterator = getStanzas().listIterator();
        int cnt = 0;
        while (iterator.hasNext()) {
            String line1 = encirclingStanzas.get(cnt);
            String line2 = encirclingStanzas.get(cnt + 1);
            String line3 = encirclingStanzas.get(cnt + 2);
            String line4 = encirclingStanzas.get(cnt + 3);
            String lastSyll1 = getLastSyllable(line1);
            String lastSyll2 = getLastSyllable(line2);
            String lastSyll3 = getLastSyllable(line3);
            String lastSyll4 = getLastSyllable(line4);
            assertEquals(lastSyll1, lastSyll4);
            assertEquals(lastSyll2, lastSyll3);
            cnt += 4;
            if (encirclingStanzas.size() <= cnt) {
                break;
            }
        }
    }
}


