package com.poem.rhymeblender.web.entity;

import com.poem.web.SpringApp;
import com.poem.web.entity.AccentEntity;
import com.poem.web.repository.GlobalDictionaryRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

import static org.junit.Assert.assertEquals;

@SpringBootTest(classes = SpringApp.class)
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase
public class GlobalDictRepoTest {

    @Autowired
    GlobalDictionaryRepository gdRepo;

    private static final String word = "эксперт";
    private static final String word_1 = "чпойньк";
    private static final AccentEntity accent = new AccentEntity(word, 4);
    private static final AccentEntity accent_1 = new AccentEntity(word_1, 2);
    private static final Set<AccentEntity> accentSet = Set.of(accent, accent_1);

    @Test
    public void jpaTest() {
        gdRepo.save(accent);
        assertEquals(accent, gdRepo.findById(word).orElse(null));
        gdRepo.deleteById(word);
        assertEquals(0, gdRepo.count());
        gdRepo.saveAll(accentSet);
        assertEquals(accent_1, gdRepo.findById(word_1).orElse(null));
        assertEquals(2, gdRepo.count());
        assertEquals((Integer) 4, gdRepo.findById(word).get().getIndex());
        gdRepo.deleteAllInBatch();
        assertEquals(0, gdRepo.count());
    }
}