package com.poem.rhymeblender.web.entity;

import com.poem.web.AccentDTO;
import com.poem.web.SpringApp;
import com.poem.web.entity.CounterAccentEntity;
import com.poem.web.repository.UserDictionaryRepository;
import com.poem.web.webdictionary.DbUserDictionary;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@SpringBootTest(classes = SpringApp.class)
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase
public class DbUserDictionaryTest {

    @Autowired
    DbUserDictionary dbUserDictionary;
    @Autowired
    UserDictionaryRepository udRepo;

    private static final String word = "мнёж" ;
    private static final Integer right_idx = 2;
    private static final Integer wrong_idx = 3;
    CounterAccentEntity accentEntity = new CounterAccentEntity(word, right_idx, 1);
    CounterAccentEntity accentEntity_1 = new CounterAccentEntity(word, wrong_idx, 1);
    AccentDTO accentDTO = new AccentDTO(word, right_idx);
    AccentDTO accentDTO_1 = new AccentDTO(word, wrong_idx);

    @Test
    public void addTest() {
        dbUserDictionary.add(word, right_idx);
        assertEquals(accentEntity, udRepo.findById(accentDTO).orElse(null));
        dbUserDictionary.add(word, wrong_idx);
        assertEquals(accentEntity_1, udRepo.findById(accentDTO_1).orElse(null));
        dbUserDictionary.add(word, right_idx);
        assertEquals(new CounterAccentEntity(word, right_idx, 2), udRepo.findById(accentDTO).orElse(null));
        dbUserDictionary.add(word, right_idx);
        assertEquals(new CounterAccentEntity(word, right_idx, 3), udRepo.findById(accentDTO).orElse(null));
    }
    @Test
    public void getTest() {
        dbUserDictionary.add(word, right_idx);
        assertEquals(right_idx, dbUserDictionary.get(word));
        dbUserDictionary.add(word, wrong_idx);
        dbUserDictionary.add(word, right_idx);
        assertEquals(right_idx, dbUserDictionary.get(word));
    }

    @Test
    public void containsTest(){
        dbUserDictionary.add(word, right_idx);
        assertTrue(dbUserDictionary.contains(word));
        dbUserDictionary.add(word, wrong_idx);
        dbUserDictionary.add(word, right_idx);
        assertTrue(dbUserDictionary.contains(word));
        assertFalse(dbUserDictionary.contains("йоу"));
    }
}