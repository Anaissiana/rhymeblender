package com.poem.rhymeblender.web.entity;

import com.poem.web.SpringApp;
import com.poem.web.TestTx;
import lombok.extern.log4j.Log4j2;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@SpringBootTest(classes = SpringApp.class)
@RunWith(SpringRunner.class)
@Log4j2
public class PGTest {

    @Autowired
    JdbcTemplate template;
    @Autowired
    TestTx tx;

    @Before
    public void init() {
        template.execute("drop table if exists accents");
        template.execute("create table if not exists accents (word varchar(20) primary key, accent_index integer)");
    }

    private int insertWord(String word, int accentIdx) {
        return template.update("insert into accents values(?, ?)", word, accentIdx);
    }

    @Test
    public void crudTest() {
        insertWord("чпуньк",1);
        assertEquals(1, countAccents());
        insertWord("ойньк", 3);
        assertEquals(1, (int) count("ойньк"));
        assertEquals(2, countAccents());
        template.update("update accents set word = 'пуньк' where word = 'чпуньк'");
        assertEquals(0, (int) count("чпуньк"));
        assertEquals(1, (int) count("пуньк"));
        template.update("update accents set accent_index = 0 where word = 'ойньк'");
        assertEquals((Integer) 0, template.queryForObject("select accent_index from accents where word = 'ойньк'", Integer.class));
        template.update("delete from accents where word = 'ойньк'");
        assertEquals(1, countAccents());
    }

    private Integer count(String word) {
        return template.queryForObject("select count(*) from accents where word = ?", Integer.class, word);
    }

    public static final String word = "пуньки";

    @Test
    public void insertTest() {
        assertEquals("Bad preparations", (Integer) 0, count(word));
        insertWord(word, 1);
        assertEquals("Couldn't insert new word", (Integer) 1, count(word));
    }

    @Test
    public void transactionalTest() {
        try {
           tx.rollback(word);
        } catch (TestTx.TxException ignored) {
        }
        assertEquals("Transaction rollback failed", 0, (int) count(word));
    }

    public int countAccents() {
        return template.queryForObject("select count(*) from accents", Integer.class);
    }
}