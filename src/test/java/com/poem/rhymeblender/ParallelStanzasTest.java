package com.poem.rhymeblender;

import com.poem.rhymeblender.stanzas.ParallelStanzas;
import com.poem.rhymeblender.stanzas.StanzasGenerationStrategy;
import org.junit.Test;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class ParallelStanzasTest extends StanzasInit {


    public ParallelStanzasTest() throws IOException {
    }

    @Override
    public StanzasGenerationStrategy getStrategy() {
        return new ParallelStanzas();
    }

    @Test
    public void makeParallelStanzasTest() {
        List<String> parallelStanzas = getStanzas();
        Iterator<String> iterator = getStanzas().listIterator();
        int cnt = 0;
        while (iterator.hasNext()) {
            String line1 = parallelStanzas.get(cnt);
            String line2 = parallelStanzas.get(cnt + 1);
            String lastSyll1 = getLastSyllable(line1);
            String lastSyll2 = getLastSyllable(line2);
            assertEquals(lastSyll1, lastSyll2);
            cnt += 2;
            if (parallelStanzas.size() <= cnt) {
                break;
            }
        }
    }
}




