package com.poem.rhymeblender;

import com.poem.rhymeblender.stanzas.CrossStanzas;
import com.poem.rhymeblender.stanzas.StanzasGenerationStrategy;
import org.junit.Test;
import java.util.Iterator;
import java.util.List;
import static org.junit.Assert.*;

public class CrossStanzasTest extends StanzasInit {

    public CrossStanzasTest() {
    }

    @Override
    public StanzasGenerationStrategy getStrategy() {
        return new CrossStanzas();
    }

    @Test
    public void makeCrossStanzasTest() {
        List<String> crossStanzas = getStanzas();
        Iterator<String> iterator = getStanzas().listIterator();
        int stanzaSize = 4;
        int cnt = 0;
        while (iterator.hasNext()) {
            String line1 = crossStanzas.get(cnt);
            String line2 = crossStanzas.get(cnt + 1);
            String line3 = crossStanzas.get(cnt + 2);
            String line4 = crossStanzas.get(cnt + 3);
            String lastSyll1 = getLastSyllable(line1);
            String lastSyll2 = getLastSyllable(line2);
            String lastSyll3 = getLastSyllable(line3);
            String lastSyll4 = getLastSyllable(line4);
            assertEquals(lastSyll1, lastSyll3);
            assertEquals(lastSyll2, lastSyll4);
            cnt += stanzaSize;
            if (crossStanzas.size() <= cnt) {
                break;
            }
        }
        assertFalse(crossStanzas.size() - cnt >= stanzaSize);
    }
}
