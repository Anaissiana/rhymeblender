package com.poem.rhymeblender;

import com.poem.rhymeblender.RhymeBlender;
import com.poem.rhymeblender.stanzas.StanzasGenerationStrategy;

import java.util.*;

import static com.poem.rhymeblender.LangUtils.getLastWord;
import static com.poem.rhymeblender.accent.PythonAccentHandler.getAccentIdx;

public abstract class StanzasInit {

    private static final char ACCENT = (char) 'z';
    protected Map<String, List<String>> rhymedLines = new HashMap<>();
    protected List<String> stanzas;

    public StanzasInit() {
        rhymedLines = generateRhymedLines();
        stanzas = getStrategy().makeStanzas(rhymedLines);
    }

    protected abstract StanzasGenerationStrategy getStrategy();

    protected List<String> getStanzas() {
        return stanzas;
    }

    public static String generateString() {
        int from = 1040;
        int to = 1102;
        int stringLength = 5;

        Random random = new Random();

        return random.ints(from, to + 1)
                .limit(stringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public static String generateLastSyllable() {
        int from = 1040;
        int to = 1103;
        int syllableLength = 3;
        Random random = new Random();

        StringBuilder buffer = new StringBuilder(syllableLength);
        for (int i = 0; i < syllableLength; i++) {
            int randomLimitedInt = from + random.nextInt(to - from);
            buffer.append((char) randomLimitedInt);
        }

        buffer.insert(1, ACCENT);
        return buffer.toString();
    }

    public String getLastSyllable(String line) {
        String lastWord = getLastWord(line);
        return lastWord.substring(lastWord.indexOf(ACCENT) - 1);
    }

    protected Map<String, List<String>> generateRhymedLines() {
        Map<String, List<String>> rhymedLines = new HashMap<>();

        for (int i = 0; i < 3; i++) {

            String lastSyll = generateLastSyllable();
            String line1 = generateString() + lastSyll;
            String line2 = generateString() + lastSyll;
            List<String> rhymedList = new ArrayList(List.of(line1, line2));
            rhymedLines.put(lastSyll, rhymedList);
            i++;
        }
        for (int i = 0; i < 3; i++) {
            String lastSyll = generateLastSyllable();
            String singleLine = generateString() + lastSyll;
            List<String> rhymedList = new ArrayList(List.of(singleLine));
            rhymedLines.put(lastSyll, rhymedList);
            i++;
        }

        for (int i = 0; i < 3; i++) {

            String lastSyll = generateLastSyllable();
            String line1 = generateString() + lastSyll;
            String line2 = generateString() + lastSyll;
            String line3 = generateString() + lastSyll;
            String line4 = generateString() + lastSyll;
            List<String> rhymedList = new ArrayList(List.of(line1, line2, line3, line4));
            rhymedLines.put(lastSyll, rhymedList);
            i++;
        }
        for (int i = 0; i < 3; i++) {

            String lastSyll = generateLastSyllable();
            String line1 = generateString() + lastSyll;
            String line2 = generateString() + lastSyll;
            String line3 = generateString() + lastSyll;
            List<String> rhymedList = new ArrayList(List.of(line1, line2, line3));
            rhymedLines.put(lastSyll, rhymedList);
            i++;
        }
        return rhymedLines;
    }
}
