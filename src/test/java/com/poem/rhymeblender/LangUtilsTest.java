package com.poem.rhymeblender;

import org.junit.Before;
import org.junit.Test;

import static com.poem.rhymeblender.LangUtils.prepareLines;
import static org.junit.Assert.assertEquals;

public class LangUtilsTest {
    protected String prepareLinesInput;
    protected String prepareLinesResult;
    protected String numLine;
    protected String onlySpaceLine;
    protected String lineWithPunc;
    protected String word;
    protected String words;

    @Before
    public void init(){
        onlySpaceLine = " ";
        lineWithPunc = ". , . / ! ";
        numLine = "123 456 789";
        word = "пуньк";
        words = "супер пуньк";
        prepareLinesInput = String.join("\n", onlySpaceLine, lineWithPunc, word, words, numLine);
        prepareLinesResult = String.join("\n", word, words);
    }

    @Test
    public void prepareLinesTest() {
        assertEquals(prepareLinesResult, prepareLines(prepareLinesInput));
    }
}
