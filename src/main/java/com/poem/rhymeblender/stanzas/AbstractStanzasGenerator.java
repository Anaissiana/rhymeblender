package com.poem.rhymeblender.stanzas;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractStanzasGenerator implements StanzasGenerationStrategy {
    private boolean needToShuffle;
    private int[] order;

    public AbstractStanzasGenerator(boolean needToShuffle, int[] order) {
        this.needToShuffle = needToShuffle;
        this.order = order;
    }

    public boolean needToShuffle() {
        return needToShuffle;
    }

    protected List<String> orderStanzas(Map<String, List<String>> rhymedLines) {
        rhymedLines = filterSingleRhyme(rhymedLines);
        List<String> stanzas = new LinkedList<>();
        List<List<String>> rhymedList = new ArrayList<>(rhymedLines.values());
        if (needToShuffle()) {
            rhymedList.forEach(Collections::shuffle);
        }
        int linesAdded = 0;
        if (rhymedList.size() > 1) {
            while (rhymedList.size() > 1) {
                if (needToShuffle()) {
                    Collections.shuffle(rhymedList);
                }
                Iterator<List<String>> rhymedListIter = rhymedList.iterator();
                while (rhymedListIter.hasNext()) {
                    List<String> first = rhymedListIter.next();
                    if (rhymedListIter.hasNext()) {
                        List<String> second = rhymedListIter.next();
                        int iterIdx = 0;
                        for (int ord : order) {
                            switch (ord) {
                                case 0:
                                    stanzas.add(first.get(iterIdx));
                                    break;
                                case 1:
                                    stanzas.add(first.get(iterIdx + 1));
                                    break;
                                case 2:
                                    stanzas.add(second.get(iterIdx));
                                    break;
                                case 3:
                                    stanzas.add(second.get(iterIdx + 1));
                                    break;
                            }
                        }
                    } else {
                        break;
                    }
                }
                linesAdded += 4;
                final int iterIdx = linesAdded / 2;
                rhymedList = rhymedList.stream().filter(a -> a.size() >= iterIdx + 2).collect(Collectors.toList());
            }
        } else {
            rhymedList.forEach(stanzas::addAll);
        }
        return stanzas;
    }

    @Override
    public List<String> makeStanzas(Map<String, List<String>> rhymedLines) {
        return orderStanzas(rhymedLines);
    }
}

