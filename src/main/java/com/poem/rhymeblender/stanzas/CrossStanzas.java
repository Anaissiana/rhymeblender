package com.poem.rhymeblender.stanzas;

public class CrossStanzas extends AbstractStanzasGenerator {

    public static final CrossStanzas INSTANCE = new CrossStanzas();

    public CrossStanzas(boolean needToShuffle, int[] order) {
        super(needToShuffle, order);
    }

    public CrossStanzas() {
        super(true, new int[]{0, 2, 1, 3});
    }
}
