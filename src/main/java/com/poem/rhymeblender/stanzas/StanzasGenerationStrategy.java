package com.poem.rhymeblender.stanzas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface StanzasGenerationStrategy {
    List<String> makeStanzas(Map<String, List<String>> map);

    default Map<String, List<String>> filterSingleRhyme(Map<String, List<String>> rhymeLines) {
        Map<String, List<String>> rhymedLines = new HashMap<>();
        for (Map.Entry<String, List<String>> entry : rhymeLines.entrySet()) {
            if (entry.getValue().size() > 1) {
                ArrayList<String> value = new ArrayList<>(entry.getValue());
                rhymedLines.put(entry.getKey(), value);
            }
        }
        return rhymedLines;
    }
}
