package com.poem.rhymeblender.stanzas;

import java.util.List;
import java.util.Map;

public enum StanzasGenerators implements StanzasGenerationStrategy {
    CROSS(CrossStanzas.INSTANCE),
    PARALLEL(ParallelStanzas.INSTANCE),
    ENCIRCLING(EncirclingStanzas.INSTANCE);

    StanzasGenerators(StanzasGenerationStrategy strategy) {
        this.strategy = strategy;
    }

    private final StanzasGenerationStrategy strategy;

    @Override
    public List<String> makeStanzas(Map<String, List<String>> map) {
        return strategy.makeStanzas(map);

    }
}