package com.poem.rhymeblender.stanzas;

public class ParallelStanzas extends AbstractStanzasGenerator {

    public static final ParallelStanzas INSTANCE = new ParallelStanzas();

    public ParallelStanzas(boolean needToShuffle, int[] order) {
        super(needToShuffle, order);
    }

    public ParallelStanzas() {
        super(true, new int[]{0, 1, 2, 3});
    }
}
