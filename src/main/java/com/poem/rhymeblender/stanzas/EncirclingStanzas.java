package com.poem.rhymeblender.stanzas;

public class EncirclingStanzas extends AbstractStanzasGenerator {

    public static final EncirclingStanzas INSTANCE = new EncirclingStanzas();

    public EncirclingStanzas(boolean needToShuffle, int[] order) {
        super(needToShuffle, order);
    }

    public EncirclingStanzas() {
        super(true, new int[]{0, 2, 3, 1});
    }

}
