package com.poem.rhymeblender;

import java.io.File;
import java.io.IOException;

public interface Accenter {

    void addAccents(File input, File output) throws IOException, InterruptedException;
}
