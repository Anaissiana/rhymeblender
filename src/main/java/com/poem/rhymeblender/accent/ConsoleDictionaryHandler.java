package com.poem.rhymeblender.accent;

import java.util.Scanner;

public class ConsoleDictionaryHandler implements DictionaryHandler {
    private final AccentDictionary accentDictionary;

    public ConsoleDictionaryHandler(AccentDictionary accentDictionary) {
        this.accentDictionary = accentDictionary;
    }

    @Override
    public Integer get(String word) {
        if (!accentDictionary.contains(word)) {
            System.out.println("Input an index of " + word);
            Scanner sc = new Scanner(System.in);
            Integer accentIdx = Integer.parseInt(sc.nextLine());
            accentDictionary.add(word, accentIdx);
        }
        return accentDictionary.get(word);
    }
}
