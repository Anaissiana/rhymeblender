package com.poem.rhymeblender.accent;

public interface DictionaryHandler {
    Integer get(String word);
}
