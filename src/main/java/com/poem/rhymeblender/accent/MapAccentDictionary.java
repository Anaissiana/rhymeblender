package com.poem.rhymeblender.accent;

import java.util.HashMap;
import java.util.Map;

public class MapAccentDictionary implements AccentDictionary {

    private Map<String, Integer> tempDict;

    public MapAccentDictionary() {
        tempDict = new HashMap<>();
    }

    public MapAccentDictionary(Map<String, Integer> dictionary) {
        this.tempDict = dictionary;
    }

    @Override
    public void flush() {
    }

    @Override
    public void add(String word, Integer accentIdx) {
        tempDict.put(word, accentIdx);
    }

    @Override
    public Integer get(String word) {
        return tempDict.get(word);
    }

    @Override
    public boolean contains(String word) {
        return tempDict.containsKey(word);
    }
}
