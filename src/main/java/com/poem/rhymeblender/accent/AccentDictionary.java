package com.poem.rhymeblender.accent;

import java.io.IOException;

public interface AccentDictionary extends DictionaryHandler {

    void add(String word, Integer accentIdx);

    boolean contains(String word);

    default void loadDictionary() throws IOException {
    }

    default void flush() throws IOException {
    }
}
