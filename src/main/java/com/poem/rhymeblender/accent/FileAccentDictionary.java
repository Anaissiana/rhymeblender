package com.poem.rhymeblender.accent;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class FileAccentDictionary implements AccentDictionary {

    private File dictFile;
    private Properties accentDict;

    public FileAccentDictionary(File dictionary) {
        this.dictFile = dictionary;
    }

    @Override
    public void loadDictionary() throws IOException {
        accentDict = new Properties();
        accentDict.load(new FileReader(dictFile));
    }

    @Override
    public void flush() throws IOException {
        accentDict.store(new FileOutputStream(dictFile), null);
    }

    @Override
    public void add(String word, Integer accentIdx) {
        accentDict.put(word, accentIdx);
    }

    @Override
    public Integer get(String word) {
        if (contains(word)) {
            return Integer.parseInt(accentDict.getProperty(word));
        } else {
            return -1;
        }
    }

    @Override
    public boolean contains(String word) {
        return accentDict.containsKey(word);
    }
}
