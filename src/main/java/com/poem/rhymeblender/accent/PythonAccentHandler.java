package com.poem.rhymeblender.accent;

import com.poem.rhymeblender.PythonAccenter;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static com.poem.rhymeblender.LangUtils.prepareLines;
import static com.poem.rhymeblender.LangUtils.splitLines;

public class PythonAccentHandler implements DictionaryHandler {

    //  private static final Locale LOCALE = new Locale("ru");
    private static final Integer ACCENT_DECIMAL = 769;
    private static final String ACCENT_SPLIT_REGEX = String.format("[^\\u%04X\\p{L}]+", ACCENT_DECIMAL & 0xFFFFF);
    private static final Character ACCENT = (char) 769;
    private static final Character DIAERESIS = (char) 'ё';
    private String input;
    private final PythonAccenter pythonAccenter;
    private Map<String, Integer> pythonAccentDict;


    public PythonAccentHandler(PythonAccenter pythonAccenter, String input) {
        this.input = input;
        this.pythonAccenter = pythonAccenter;
    }

    public static String getLastWordsIgnoreAccents(String line) {
        line = line.toLowerCase();
        String[] withoutPunc = line.split(ACCENT_SPLIT_REGEX);
        return withoutPunc[withoutPunc.length - 1];
    }

    private void accentDictInit() {
        List<String> lastWordList = new ArrayList<>();
        try {
            lastWordList = getLastWordsList(input);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        for (String word : lastWordList) {
            int accentIdx = getAccentIdx(word);
            pythonAccentDict.put(word.replace(ACCENT.toString(), ""), accentIdx);
        }
    }

    @Override
    public Integer get(String word) {
        if (pythonAccentDict == null) {
            pythonAccentDict = new HashMap<>();
            accentDictInit();
        }
        return pythonAccentDict.get(word);
    }

    public static int getAccentIdx(String lastWord) {
        if (lastWord.contains(DIAERESIS.toString())) {
            return lastWord.indexOf(DIAERESIS);
        }
        if (!lastWord.contains(ACCENT.toString())) {
            return -1;
        }
        return lastWord.indexOf(ACCENT_DECIMAL) - 1;
    }

    private File makeAccentFile(String input) throws IOException, InterruptedException {
        String hash = String.valueOf(input.hashCode());
        String tmpDir = System.getProperty("java.io.tmpdir");
        File inputFile = Paths.get(tmpDir, hash + ".txt").toFile();
        File outputFile = Paths.get(tmpDir, hash + ".accent").toFile();
        if (!outputFile.exists()) {
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(inputFile))) {
                bw.write(input);
            }
            pythonAccenter.addAccents(inputFile, outputFile);
        }
        return outputFile;
    }

    private List<String> getLastWordsList(String input) throws IOException, InterruptedException {
        List<String> splitLines = prepareLines(splitLines(input));
        Set<String> uniqueWords = new HashSet<>();
        for (String line : splitLines) {
            String lastWord = getLastWordsIgnoreAccents(line);
            uniqueWords.add(lastWord);
        }
        String lineBreakLastWords = String.join("\n", uniqueWords);
        File accentFile = makeAccentFile(lineBreakLastWords);
        return Files.readAllLines(Paths.get(String.valueOf(accentFile.toPath())));
    }
}