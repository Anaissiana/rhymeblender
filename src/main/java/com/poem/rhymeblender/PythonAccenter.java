package com.poem.rhymeblender;

import java.io.*;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class PythonAccenter implements Accenter {
    private final String accentScriptPath;

    public PythonAccenter(String accentScriptPath) {
        this.accentScriptPath = accentScriptPath;
    }

    private static class StreamGobbler implements Runnable {
        private InputStream inputStream;
        private Consumer<String> consumer;

        public StreamGobbler(InputStream inputStream, Consumer<String> consumer) {
            this.inputStream = inputStream;
            this.consumer = consumer;
        }

        @Override
        public void run() {
            new BufferedReader(new InputStreamReader(inputStream)).lines()
                    .forEach(consumer);
        }
    }

    @Override
    public void addAccents(File input, File output) throws IOException, InterruptedException {
        ProcessBuilder builder = new ProcessBuilder();
        builder.command(accentScriptPath, input.getAbsolutePath(), output.getAbsolutePath());

        builder.directory(new File(System.getProperty("user.home")));
        Process process = builder.start();
        StreamGobbler streamGobbler =
                new StreamGobbler(process.getInputStream(), System.out::println);
        StreamGobbler errGobbler =
                new StreamGobbler(process.getErrorStream(), System.out::println);
        Executors.newSingleThreadExecutor().submit(streamGobbler);
        Executors.newSingleThreadExecutor().submit(errGobbler);
        int exitCode = process.waitFor();
        if (exitCode != 0) {
            throw new RuntimeException("Accent file generation failed");
        }
    }
}