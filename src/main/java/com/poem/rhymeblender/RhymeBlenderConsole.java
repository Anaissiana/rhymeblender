package com.poem.rhymeblender;

import com.poem.rhymeblender.accent.ConsoleDictionaryHandler;
import com.poem.rhymeblender.accent.FileAccentDictionary;

import com.poem.rhymeblender.stanzas.StanzasGenerationStrategy;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.io.*;
import java.util.Scanner;
import java.util.concurrent.Callable;

@Command(name = "poem blender",
        description = "mixes input poem lines into shuffled rhymed stanzas")
public class RhymeBlenderConsole implements Callable<Integer> {

    @Option(names = {"-a", "--accentScript"}, required = true, paramLabel = "ACCENT SCRIPT PATH", description = "accent script path")
    String accentScriptPath;
    @Option(names = {"-i", "--input"}, required = true, paramLabel = "INPUT FILE", description = "input file to mix")
    File inputFile;
    @Option(names = {"-o", "--output"}, paramLabel = "OUTPUT FILE", description = "output file")
    File outputFile;
    @Option(names = {"-d", "--dictionaryPath"}, paramLabel = "DICTIONARY PATH", description = "path to dictionary")
    File dictionaryPath;
    @Option(names = {"-s", "--strategy"}, required = true, paramLabel = "STRATEGY")
    StanzasGenerationStrategy strategy;
    @Option(names = {"-h", "--help"}, usageHelp = true, description = "display a help message")
    private boolean helpRequested;

    private Accenter accenter;

    public static void main(String[] args) {
        RhymeBlenderConsole rhymeBlenderApp = new RhymeBlenderConsole();
        int exitCode = new CommandLine(rhymeBlenderApp).execute(args);
        System.exit(exitCode);
    }

    @Override
    public Integer call() throws Exception {
        accenter = new PythonAccenter(accentScriptPath);
        accenter.addAccents(inputFile, outputFile);
        OutputStream out = System.out;
        if (dictionaryPath == null) {
            dictionaryPath = File.createTempFile("temp_dict_file", ".txt");
        }
        FileAccentDictionary d = new FileAccentDictionary(dictionaryPath);
        d.loadDictionary();
        ConsoleDictionaryHandler accentDict = new ConsoleDictionaryHandler(d);
        RhymeBlender rhymeBlender = new RhymeBlender(accentDict, inputFile);
        if (outputFile != null) {
            out = new FileOutputStream(outputFile);
        }
        int exitCode = 0;
        try {
            if (inputFile != null) {
                Scanner in = new Scanner(System.in);
                String inputStanzas = in.nextLine();
                rhymeBlender.mix(strategy, out);
            }
        } catch (FileNotFoundException e) {
            System.err.println("the input file wasn't found");
            return 2;
        } catch (IOException e) {
            System.err.println("I/O exception occurred");
            e.printStackTrace();
            return 3;
        } finally {
            try {
                if (outputFile != null) {
                    out.close();
                }
            } catch (IOException e) {
                System.err.println("the output file closing failed");
                e.printStackTrace();
                exitCode = 1;
            }
        }
        return exitCode;
    }
}