package com.poem.rhymeblender;

import com.poem.rhymeblender.accent.AccentDictionary;
import com.poem.rhymeblender.accent.DictionaryHandler;
import com.poem.rhymeblender.accent.MapAccentDictionary;
import com.poem.rhymeblender.stanzas.StanzasGenerationStrategy;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.*;

import static com.poem.rhymeblender.LangUtils.*;

public class RhymeBlender implements RhymeBlenderService {

    protected DictionaryHandler dictHandler;
    protected AccentDictionary localDict;
    protected Set<String> withoutAccents = new HashSet<>();
    protected String input;
    protected List<String> preparedInput;

    public RhymeBlender(DictionaryHandler dictionary, String input) {
        this.dictHandler = dictionary;
        this.input = input;
    }

    public RhymeBlender(DictionaryHandler dictionary, File input) throws IOException {
        this(dictionary, new String(Files.readAllBytes(input.toPath())));
    }

    Map<String, List<String>> getRhymedLines() {
        getWithoutAccents();
        Map<String, List<String>> rhymeLines = new HashMap<>();
        for (String line : preparedInput) {
            String ending = getLastSyllableOrDict(line);
            if (ending == null) {
                continue;
            }
            if (!rhymeLines.containsKey(ending)) {
                rhymeLines.put(ending, new ArrayList<>());
            }
            rhymeLines.get(ending).add(line);
        }
        return rhymeLines;
    }

    private String getLastSyllableOrDict(String line) {
        String lastWord = getLastWord(line);
        return Optional.ofNullable(localDict.get(lastWord)).map(lastWord::substring).orElse(null);
    }

    public Set<String> getWithoutAccents() {
        if (localDict == null) {
            localDict = new MapAccentDictionary();
            preparedInput = prepareLines(splitLines(input));
            input = null;
            for (String line : preparedInput) {
                String lastWord = getLastWord(line);
                int accentIdx = dictHandler.get(lastWord);
                if (accentIdx == -1) {
                    withoutAccents.add(lastWord);
                } else {
                    localDict.add(lastWord, accentIdx);
                }
            }
        }
        return withoutAccents;
    }

    @Override
    public void mix(StanzasGenerationStrategy strategy, OutputStream output)
            throws IOException {
        output.write(mix(strategy).getBytes(StandardCharsets.UTF_8));
    }

    @Override
    public String mix(StanzasGenerationStrategy strategy) {
        return String.join("\n", strategy.makeStanzas(getRhymedLines()));
    }
}