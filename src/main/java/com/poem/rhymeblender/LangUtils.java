package com.poem.rhymeblender;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class LangUtils {
    private static final String SPLIT_REGEX = "[^\\p{L}]+";

    private LangUtils() {
    }

    public static String getLastWord(String line) {
        line = line.toLowerCase();
        String[] withoutPunc = line.split(SPLIT_REGEX);
        return withoutPunc[withoutPunc.length - 1];
    }

    public static String[] splitLines(String input) {
        return input.split("\n");
    }

    public static String prepareLines(String input) {
        String[] splitLines = splitLines(input);
        List<String> preparedInput = new ArrayList<>();
        for (String line : splitLines) {
            if (!line.matches(SPLIT_REGEX)) {
                preparedInput.add(line);
            }
        }
        return String.join("\n", preparedInput);
    }

    public static List<String> prepareLines(List<String> lines){
        return lines.stream().filter(line -> !line.matches(SPLIT_REGEX)).collect(Collectors.toList());
    }

    public static List<String> prepareLines(String[] lines){
        return prepareLines(Arrays.asList(lines));
    }
}