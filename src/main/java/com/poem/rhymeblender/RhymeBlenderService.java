package com.poem.rhymeblender;

import com.poem.rhymeblender.stanzas.StanzasGenerationStrategy;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

public interface RhymeBlenderService {

    void mix(StanzasGenerationStrategy strategy, OutputStream output) throws IOException;

    String mix(StanzasGenerationStrategy strategy);
}
