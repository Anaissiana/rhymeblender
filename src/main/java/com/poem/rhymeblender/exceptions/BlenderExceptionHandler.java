package com.poem.rhymeblender.exceptions;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@Log4j2
@ControllerAdvice
public class BlenderExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex) {
        List<String> details = new ArrayList<>();
        details.add(ex.getMessage());
        ErrorResponse error = new ErrorResponse("Server Error", details);
        log.error("Unknown error occurred", ex);
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(PoemNotFoundException.class)
    public static ResponseEntity<Object> handlePoemNotFoundException(PoemNotFoundException poemNotFoundException) {
        List<String> details = new ArrayList<>();
        details.add(poemNotFoundException.getLocalizedMessage());
        ErrorResponse error = new ErrorResponse("Poem Not Found", details);
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidAccentIndexException.class)
    public static ResponseEntity<Object> handleInvalidAccentException(InvalidAccentIndexException invalidAccentIndexException) {
        List<String> details = new ArrayList<>();
        details.add(invalidAccentIndexException.getLocalizedMessage());
        ErrorResponse error = new ErrorResponse("Invalid accent index input", details);
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidDictionaryWordException.class)
    public static ResponseEntity<Object> handleInvalidDictionaryWordException(InvalidDictionaryWordException InvalidDictionaryWordException) {
        List<String> details = new ArrayList<>();
        details.add(InvalidDictionaryWordException.getLocalizedMessage());
        ErrorResponse error = new ErrorResponse("Invalid dictionary word input", details);
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> details = new ArrayList<>();
        for (ObjectError error : ex.getBindingResult().getAllErrors()) {
            details.add(error.getDefaultMessage());
        }
        ErrorResponse error = new ErrorResponse("Validation Failed", details);
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}