package com.poem.rhymeblender.exceptions;

public class BlenderException extends RuntimeException {

    public BlenderException(String exception) {
        super(exception);
    }
}