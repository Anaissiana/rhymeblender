package com.poem.rhymeblender.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidDictionaryWordException extends BlenderException {

    public InvalidDictionaryWordException(String exception) {
        super(exception);
    }
}