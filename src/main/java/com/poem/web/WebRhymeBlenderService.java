package com.poem.web;

import com.poem.rhymeblender.Accenter;
import com.poem.rhymeblender.RhymeBlender;
import com.poem.rhymeblender.accent.DictionaryHandler;
import com.poem.rhymeblender.accent.PythonAccentHandler;
import com.poem.rhymeblender.stanzas.StanzasGenerationStrategy;
import com.poem.rhymeblender.stanzas.StanzasGenerators;
import com.poem.web.webdictionary.*;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Objects;
import java.util.Set;

@Service
public class WebRhymeBlenderService {

    @Autowired
    private ObjectProvider<PythonAccentHandler> pythonAccentHandlerProvider;
    @Autowired
    DbPythonDictionary dbPythonDictionary;
    @Autowired
    DbUserDictionary dbUserDictionary;

    @Autowired
    private Accenter accenter;
    private static final StanzasGenerationStrategy DEFAULT_STRATEGY = StanzasGenerators.PARALLEL;

    public MixPoemResponse mix(PoemData poemData) {
        String input = poemData.getPoem();
        Map<String, Integer> userAccents = poemData.getAccents();
        UserAccentDictionary userAccentDictionary = null;
        if (userAccents != null) {
            userAccentDictionary = new UserAccentDictionary(userAccents);
        }
        PythonAccentHandler pythonAccentHandler = pythonAccentHandlerProvider.getObject(input);
        DictionaryHandler pythonHandler = new PythonHandlerDbProxy(pythonAccentHandler, dbPythonDictionary);
        WebDictHandler webDictHandler = new WebDictHandler(userAccentDictionary, pythonHandler, dbUserDictionary);
        RhymeBlender rhymeBlender = new RhymeBlender(webDictHandler, input);
        Set<String> withoutAccents = rhymeBlender.getWithoutAccents();
        StanzasGenerators stanzasStrategy = poemData.getStanzasStrategy();
        String blenderResult;
        blenderResult = rhymeBlender.mix(Objects.requireNonNullElse(stanzasStrategy, DEFAULT_STRATEGY));
        if (!withoutAccents.isEmpty()) {
            return MixPoemResponse.fromMixResultAndWithoutAccents(withoutAccents, blenderResult);
        }
        return MixPoemResponse.fromMixResult(blenderResult);
    }
}