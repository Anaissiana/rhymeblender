package com.poem.web.repository;

import com.poem.web.AccentDTO;
import com.poem.web.entity.CounterAccentEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDictionaryRepository extends JpaRepository<CounterAccentEntity, AccentDTO> {

    @Query("select cae.index from CounterAccentEntity cae where cae.word = :word order by(cae.counter) desc")
    List<Integer> getIdxWithMaxCounter(@Param("word") String word, Pageable pageable);
}