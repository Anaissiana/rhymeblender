package com.poem.web.repository;

import com.poem.web.entity.AccentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GlobalDictionaryRepository extends JpaRepository<AccentEntity, String> {
}