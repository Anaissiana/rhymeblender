package com.poem.web;

import com.poem.web.entity.Accent;
import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class AccentDTO implements Accent, Serializable {
    private String word;
    private Integer index;
}
