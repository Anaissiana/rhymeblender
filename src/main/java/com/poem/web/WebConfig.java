package com.poem.web;


import com.poem.rhymeblender.PythonAccenter;
import com.poem.rhymeblender.accent.PythonAccentHandler;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;

@Configuration
//@PropertySource("classpath:application-natal.properties")
@EnableTransactionManagement
@Log4j2
@ComponentScan(basePackages = "com.poem.rhymeblender")
public class WebConfig {
    @Value("${accenter.python.scriptpath}")
    private String pythonAccenterPath;

    @Bean
    public PythonAccenter pythonAccenter() throws FileNotFoundException {
        log.trace("Checking python accenter...");
        if (Files.exists(Path.of(pythonAccenterPath))) {
            return new PythonAccenter(pythonAccenterPath);
        } else {
            if (Strings.isEmpty(pythonAccenterPath)) {
                log.warn("Python accenter path not set");
                return null;
            } else {
                String err = String.format("Python accenter path set but file not found: %s", pythonAccenterPath);
                log.error(err);
                throw new FileNotFoundException(err);
            }
        }
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public PythonAccentHandler pythonAccentHandler(String input) throws FileNotFoundException {
        return new PythonAccentHandler(pythonAccenter(), input);
    }
}