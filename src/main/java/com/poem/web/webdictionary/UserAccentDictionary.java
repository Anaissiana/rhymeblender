package com.poem.web.webdictionary;

import com.poem.rhymeblender.accent.AccentDictionary;

import java.util.Map;

public class UserAccentDictionary implements AccentDictionary {
    private Map<String, Integer> accents;

    public UserAccentDictionary(Map<String, Integer> accents) {
        this.accents = accents;
    }

    @Override
    public void add(String word, Integer accentIdx) {
        accents.put(word, accentIdx);
    }

    @Override
    public Integer get(String word) {
        return accents.get(word);
    }

    @Override
    public boolean contains(String word) {
        boolean hasAccent = false;
        if (accents != null) {
            hasAccent = accents.containsKey(word);
        }
        return hasAccent;
    }
}
