package com.poem.web.webdictionary;

import com.poem.rhymeblender.accent.AccentDictionary;
import com.poem.web.entity.AccentEntity;
import com.poem.web.repository.GlobalDictionaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class DbPythonDictionary implements AccentDictionary {

    @Autowired
    GlobalDictionaryRepository gdRepo;

    @Override
    public void add(String word, Integer accentIdx) {
        AccentEntity accent = new AccentEntity(word, accentIdx);
        gdRepo.save(accent);
    }

    @Override
    public boolean contains(String word) {
        return gdRepo.existsById(word);
    }

    @Override
    public Integer get(String word) {
        Optional<AccentEntity> accent = gdRepo.findById(word);
        if (accent.isPresent()) {
            return accent.get().getIndex();
        }
        return -1;
    }
}