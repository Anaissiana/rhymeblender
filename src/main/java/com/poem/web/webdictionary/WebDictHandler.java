package com.poem.web.webdictionary;

import com.poem.rhymeblender.accent.DictionaryHandler;

public class WebDictHandler implements DictionaryHandler {

    private final UserAccentDictionary userAccentDict;
    private final DictionaryHandler pythonAccentHandler;
    private final DbUserDictionary dbUserDictionary;

    public WebDictHandler(UserAccentDictionary userAccentDict) {
        this.userAccentDict = userAccentDict;
        this.pythonAccentHandler = null;
        this.dbUserDictionary = null;
    }

    public WebDictHandler(UserAccentDictionary userAccentDict, DictionaryHandler pythonAccentHandler, DbUserDictionary dbUserDictionary) {
        this.userAccentDict = userAccentDict;
        this.pythonAccentHandler = pythonAccentHandler;
        this.dbUserDictionary = dbUserDictionary;
    }

    @Override
    public Integer get(String word) {
        if (userAccentDict != null) {
            Integer accentIdx = userAccentDict.get(word);
            if (accentIdx != null) {
                if (dbUserDictionary != null) {
                    dbUserDictionary.add(word, accentIdx);
                }
                return accentIdx;
            }
        }
        int pythonIdx = 0;
        if (pythonAccentHandler != null) {
            pythonIdx = pythonAccentHandler.get(word);
        }
        if (pythonIdx != -1) {
            return pythonIdx;
        } else return dbUserDictionary.get(word);
    }
}



