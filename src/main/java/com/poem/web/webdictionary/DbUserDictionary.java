package com.poem.web.webdictionary;

import com.poem.rhymeblender.accent.AccentDictionary;
import com.poem.web.AccentDTO;
import com.poem.web.entity.CounterAccentEntity;
import com.poem.web.repository.UserDictionaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Component
public class DbUserDictionary implements AccentDictionary {

    @Autowired
    UserDictionaryRepository udRepo;
    private static final Pageable one = PageRequest.of(0, 1);


    @Transactional
    @Override
    public void add(String word, Integer accentIdx) {
        AccentDTO accentDTO = new AccentDTO(word, accentIdx);
        CounterAccentEntity accentEntity = udRepo.findById(accentDTO).orElse(null);
        if (accentEntity != null) {
            Integer counter = Objects.requireNonNull(accentEntity).getCounter();
            accentEntity.setCounter(++counter);
        } else {
            accentEntity = new CounterAccentEntity(word, accentIdx, 1);
            udRepo.save(accentEntity);
        }
    }

    @Override
    public boolean contains(String word) {
        return !udRepo.getIdxWithMaxCounter(word, one).isEmpty();
    }

    @Override
    public Integer get(String word) {
        List<Integer> counterIdx = udRepo.getIdxWithMaxCounter(word, one);
        if (!counterIdx.isEmpty()) {
            return counterIdx.get(0);
        } else return -1;
    }
}