package com.poem.web.webdictionary;

import com.poem.rhymeblender.accent.DictionaryHandler;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PythonHandlerDbProxy implements DictionaryHandler {
    //Composition over inheritance(Ef. Java item)
    //Proxy pattern
    private final DictionaryHandler pythonDictionary;
    private final DbPythonDictionary dbPythonDictionary;

    @Override
    public Integer get(String word) {
        Integer accentIdx = dbPythonDictionary.get(word);
        if (accentIdx == null) {
            accentIdx = pythonDictionary.get(word);
            dbPythonDictionary.add(word, accentIdx);
            return accentIdx;
        } else {
            return accentIdx;
        }
    }
}
