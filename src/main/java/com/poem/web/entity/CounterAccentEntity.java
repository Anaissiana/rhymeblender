package com.poem.web.entity;

import com.poem.web.AccentDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "counter_accent")
@Data
@NoArgsConstructor
@AllArgsConstructor
@IdClass(AccentDTO.class)
public class CounterAccentEntity implements Accent {

    @Id
    @Column(name = "word", updatable = false, nullable = false)
    private String word;
    @Id
    @Column(name = "index", nullable = false, updatable = false)
    private Integer index;

    @Column(name = "counter", nullable = false, columnDefinition = "integer default 0")
    private Integer counter;

}