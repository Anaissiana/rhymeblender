package com.poem.web.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "accents", indexes = @Index(columnList = "index"))
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccentEntity implements Accent {

    @Id
    @Column(updatable = false, nullable = false)
    private String word;
    @Column(name = "index", nullable = false)
    private Integer index;
}