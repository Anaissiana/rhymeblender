package com.poem.web.entity;

public interface Accent {
    String getWord();
    Integer getIndex();
}
