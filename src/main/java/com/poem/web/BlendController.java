package com.poem.web;

import com.poem.rhymeblender.exceptions.InvalidAccentIndexException;
import com.poem.rhymeblender.exceptions.InvalidDictionaryWordException;
import com.poem.web.repository.GlobalDictionaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;

@RestController
public class BlendController {
    @Autowired
    WebRhymeBlenderService webRhymeBlenderService;

    @PostMapping("/blend")
    public ResponseEntity<MixPoemResponse> blend(
            @RequestBody @Valid PoemData poemData) {
        if (poemData.getAccents() != null) {
            validateUserDictInput(poemData.getAccents());
        }
        MixPoemResponse result = webRhymeBlenderService.mix(poemData);
        System.out.println(result);
        return ResponseEntity.ok(result);
    }

    public static void validateUserDictInput(Map<String, Integer> accents) {
        String regex = "[\\p{L}]+";
        for (Integer accentIdx : accents.values()) {
            if (accentIdx < 0) {
                throw new InvalidAccentIndexException("Accent indexes should be only positive or zero");
            }
        }
        for (String word : accents.keySet()) {
            if (!word.matches(regex)) {
                throw new InvalidDictionaryWordException("Dictionary words should contain letters of the alphabet");
            }
        }
    }
}