package com.poem.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class TestTx {

    @Autowired
    JdbcTemplate template;

    public static class TxException extends Exception {
    }

    @Transactional(rollbackFor = {TxException.class})
    public void rollback(String word) throws TxException {
        if (insertWord(word, 1) != 1) {
            throw new IllegalStateException("Did not add word");
        }
        throw new TxException();
    }

    private int insertWord(String word, int accentIdx) {
        return template.update("insert into accents values(?, ?)", word, accentIdx);
    }
}
