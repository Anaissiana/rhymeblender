package com.poem.web;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MixPoemResponse {

    public String blenderResult;
    public Set<String> withoutAccents;

    @Override
    public String toString() {
        return "Your blender result:\n" + blenderResult + '\n' +
                "Unstressed word list is " + withoutAccents;
    }

    public static MixPoemResponse withoutAccents(Set<String> withoutAccents) {
        return new MixPoemResponse(withoutAccents);
    }

    public static MixPoemResponse fromMixResult(String blenderResult) {
        return new MixPoemResponse(blenderResult);
    }

    public static MixPoemResponse fromMixResultAndWithoutAccents(Set<String> withoutAccents, String blenderResult){
        return new MixPoemResponse(withoutAccents, blenderResult);
    }

    private MixPoemResponse(Set<String> withoutAccents) {
        this.withoutAccents = withoutAccents;
    }

    private MixPoemResponse(String blenderResult) {
        this.blenderResult = blenderResult;
    }

    private MixPoemResponse(Set<String> withoutAccents, String blenderResult){
        this.withoutAccents = withoutAccents;
        this.blenderResult = blenderResult;
    }
}