package com.poem.web;

import com.poem.rhymeblender.stanzas.StanzasGenerators;

import javax.validation.constraints.NotEmpty;
import java.util.Map;

public class PoemData {

    @NotEmpty(message = "Poem should not be empty")
    private String poem;
    private Map<String, Integer> accents;
    private StanzasGenerators stanzasStrategy;

    public PoemData() {
    }

    public String getPoem() {
        return poem;
    }

    public void setPoem(String poem) {
        this.poem = poem;
    }

    public Map<String, Integer> getAccents() {
        return accents;
    }

    @Override
    public String toString() {
        return "PoemData{" +
                "poem='" + poem + '\'' +
                ", accents=" + accents +
                ", stanzasStrategy=" + stanzasStrategy +
                '}';
    }

    public void setAccents(Map<String, Integer> accents) {
        this.accents = accents;
    }

    public StanzasGenerators getStanzasStrategy() {
        return stanzasStrategy;
    }

    public void setStanzasStrategy(StanzasGenerators stanzasStrategy) {
        this.stanzasStrategy = stanzasStrategy;
    }
}

